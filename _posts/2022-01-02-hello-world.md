---
layout: post
title: "Hello World"
date: 2022-01-02 15:55:22
img: 2022-01-02-hello-world/cover-helloWorld.png #add image name.png|jpg|etc (image from _assets/img/posts)
description: ''
fig-caption:
tags: [blog]
author: 'Miguel Rosa'
---


# Hello (my) World!
Depois de alguns anos parado na blogosfera e de muita ponderação (e algum apoio), eis que decidi voltar a escrever!
O presente blog está muito longe dos sistemas utilizados anteriormente (blog no extinto *myopera* e *wordpress*), uma vez que deixei de utilizar sistemas complexos recurrendo a base de dados remotas, configurações super-hiper-mega complicadas.

Assim, o presente cantinho conta com a minha visão/opinião sobre os temas apresentados, partilhando também vários guias e/ou tutoriais, artigos esses que podem (ou não) surgir em formato vídeo no meu canal do youtube (basta clicar no icon no canto inferior esquerdo).

<br>

### Porquê ***Tux Overflow***?
O título é uma junção de dois mundos que gosto e que, de certa forma, estão interligados: Linux e Programação.

Linux (ou a sua mascote, o *Tux*) é o sistema operativo que uso desde há alguns anos (primeiro contato foi em 2002 - se não estou em erro), sendo que é o sistema operativo em todas as minhas máquinas. Já passei por algumas distribuições, mas de momento estou a utilizar/focado no Slackware.

Já no que toca à programação, o termo *overflow* vem quando é utilizado/incrementado um valor a uma pilha de memória e esta, por consequente "rebenta" provocando um erro de overflow (excesso de informação). Como o conhecimento não tem limite, é complicado chegar a um overflow, pelo que achei interessante a junção dos nomes (Tux + Overflow) e assim dar início não só ao nome do projeto como também a esta caminhada.


<br>

### Pronto ok, e que mais?
Sou um curioso e apaixonado por tecnologia desde muito cedo, fazendo com que esse fosse o meu percurso profissional, a área da Informática. Sou utilizador, adepto/defensor do FOSS (**F**ree **O**pen **S**ource **S**oftware) quer a nível de Sistema Operativo, quer a nível de ferramentas que uso no dia a dia. No que se refere a equipamento, irei falar num próximo post. Ainda em relação ao FOSS, sou membro associado da ANSOL (**A**ssociação **N**acional para o **So**ftware **L**ivre) que é uma associação sem fins lucrativos que tem como principais objetivos a divulgação, promoção, desenvolvimento, investigação e estudo da informática livre a nivel nacional (Portugal).

Após esta breve apresentação, espero vê-lo mais vezes neste canto virtual.